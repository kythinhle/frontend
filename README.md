# Front-end structure

[![N|Solid](https://cdn.itviec.com/employers/ven-creative/logo/w170/SU525kXbzidEzvPzA1reTtCA/ven-creative-logo.png)](https://nodesource.com/products/nsolid)

## How do I get set up
### Install nodejs

`https://nodejs.org/en/download/releases/`

### Install gulpjs global
Open command line and run:
```sh
$ npm install --global gulp-cli
```
Website: `https://gulpjs.com/`

### Run your project
Install node_modules
```sh
$ npm install
```
```sh
$ yarn
```
Build source
```sh
$ npm run build
```
```sh
$ yarn build
```
Watch source
```sh
$ npm start
```
```sh
$ yarn start
```
Update images
```sh
$ npm run images
```
```sh
$ yarn images
```
Update plugins
```sh
$ npm run plugins
```
```sh
$ yarn plugins
```

> NOTE:
> When you add more images or js library.
> You need run command line for each.

## Structure

| Folder | Description |
| ------ | ------ |
| dist | The place source code was build. Don't working here. Please ignore it. |
| src | The place for coding |
| src/core | Core |
| src/_core/layout | Settings your layout, your mixins, your icons |
| src/_core/styles | Settings variables style |
| src/assets | Your assets |
| src/components | Your components (Header, Footer ...) |
| src/objects | Your objects (Buttons, Icons, Form, Heading ...) |
| src/pages | Your pages (Home, About, Contact) |
| src/main.js | Main javascript file. Init common function |
| src/main.scss | Main styles. Include all styles |
| src/global.scss | Global styles. |

## Extenstion helpful for VS code  ###

1. Prettier Code Formatter
2. Pug (Jade) snippets
3. Pug\Jade Bootstrap, Font Awesome snippets
4. Sass