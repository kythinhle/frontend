// Global functions
var scrollbarWidth = window.innerWidth - document.documentElement.clientWidth;

function openPopupOverlay(speed = 300) {
    if ($('.ven-popup-overlay').length) return;
    $('body').append('<div class="ven-popup-overlay"></div>');
    $('body').addClass('is-lock').css('paddingRight', scrollbarWidth);
    $('.ven-popup-overlay').fadeIn(speed);
}

function closePopupOverlay(speed = 300) {
    $('.ven-popup-overlay').fadeOut(speed);
    setTimeout(function () {
        $('.ven-popup-overlay').remove();
    }, speed);
    $('body').removeClass('is-lock').css('padding-right', '');
}

function getRootVars() {
    var root = document.querySelector(":root");
    root.style.setProperty("--vh", window.innerHeight / 100 + "px");
}

// Main functions
(function ($) {
    $.ven_noti = function (html, time = 2500) {
        if ($('.ven-noti').length) return;
        $('body').append('<div class="ven-noti">' + html + '</div>');
        setTimeout(function () {
            $('.ven-noti').addClass('opening');
        }, 10);
        setTimeout(function () {
            $('.ven-noti').removeClass('opening');
        }, time);
        setTimeout(function () {
            $('.ven-noti').remove();
        }, time + 400);
    };

    function handleWordpressAdminMode() {
        if ($('#wpadminbar').length && $('.js-ven-navbar').length && $(window).width() <= 600) {
            $(window).on('scroll', function () {
                var top = $(window).scrollTop(),
                    offsetTop = 46 - top > 0 ? 46 - top : 0;
                $('.js-ven-navbar').css('margin-top', offsetTop);
            });
        }
    }

    function initLazyLoad() {
        $('.lazy').Lazy({
            afterLoad: function (el) {
                $(el).addClass('loaded');
                // handleIE();
            }
        });
    }

    function initSelect2() {
        $('.ginput_container_select select').select2({
            width: "100%",
            minimumResultsForSearch: -1
        });
    }

    function initPopup() {
        $('[data-popup-target]').on('click', function (e) {
            e.preventDefault();
            var popupTarget = $(this).data('popup-target'),
                popupContent = $('[data-popup-content="' + popupTarget + '"]');
            if (popupContent.length == 0) return;
            popupContent.addClass('is-active');
            openPopupOverlay();
        });

        $('[data-popup-close]').on('click', function (e) {
            e.preventDefault();
            $(this).closest('[data-popup-content]').removeClass('is-active');
            closePopupOverlay();
        });

        $(document).on('click', '.ven-popup-overlay', function (e) {
            $('[data-popup-content]').removeClass('is-active');
            closePopupOverlay();
        });
    }

    function handleIE() {
        var userAgent, ieReg, ie;
        userAgent = window.navigator.userAgent;
        ieReg = /msie|Trident.*rv[ :]*11\./gi;
        ie = ieReg.test(userAgent);

        if (ie) {
            $('.ven-img-drop').each(function () {
                var $container = $(this),
                    imgLazy = $(this).find('img').attr('src'),
                    picLazy = $(this).find('source').attr('srcset'),
                    imgUrl = picLazy ? picLazy : imgLazy;
                if (imgUrl) {
                    $container.css('backgroundImage', 'url(' + imgUrl + ')').addClass('custom-object-fit');
                }
            });
        }
    }

    function initAnchorScroll() {
        $('a.js-anchor-scroll[href*=\\#]:not([href=\\#])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                || location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    }

    function initFormFloatLabel() {
        $('.ginput_container_select select').closest('.gfield').addClass('has-select');
        $('.ven-form-group select').closest('.ven-form-group').addClass('has-select');

        var formFields = $('.gfield, .ven-form-group');
        formFields.each(function () {
            var field = $(this);
            var input = field.find('input:not([type="radio"]):not([type="checkbox"]), textarea');
            var label = field.find('label');

            if(input.attr('type') != 'file') {
                input.focus(function () {
                    label.addClass('freeze');
                });
            }

            input.focusout(function () {
                checkInput();
            });

            if (input.val() && input.val().length) {
                label.addClass('freeze');
            }

            function checkInput() {
                var valueLength = input.val().length;

                if (valueLength > 0) {
                    label.addClass('freeze');
                } else {
                    label.removeClass('freeze');
                }
            }

            input.change(function () {
                checkInput();
            });
        });
    }

    $(function () {
        getRootVars();
        initLazyLoad();
        initSelect2();
        initPopup();
        handleWordpressAdminMode();
        // initFormFloatLabel();
        // initAnchorScroll();
    });

    $(window).on('resize', function() {
        getRootVars();
    });
})(jQuery);