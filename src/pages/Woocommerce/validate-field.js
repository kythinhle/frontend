(function ($) {
 	/**
    * Check if input, select is valid
    */
    $.validateInput = function (e, countError) {
        var wrapper = $(e).find('.woocommerce-input-wrapper'),
            input = $(e).find('input'),
            select = $(e).find('select'),
            label = $(e).find('label').clone()    //clone the element
                .children() //select all the children
                .remove()   //remove all the children
                .end()  //again go back to selected element
                .text(),
            error_msg = $(e).find('.invalid-message');


        if (input.val() || select.val()) {
            wrapper.removeClass('ven-form-group--error');
            error_msg.remove();

            //Check email
            if(input.attr('id') == "shipping_email" || input.attr('id') == "billing_email") {
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!regex.test(input.val())) {
                    if (!wrapper.hasClass('ven-form-group--error')) {
                        wrapper.append('<div class="invalid-message">Please enter a valid email address.</div>');
                    }
                    wrapper.addClass('ven-form-group--error');
                    countError = parseInt(countError) + 1;
                }
            }

            //Check phone
            if(input.attr('id') == "shipping_phone" || input.attr('id') == "billing_phone") {
                
                if (isNaN(input.val())) {
                    if (!wrapper.hasClass('ven-form-group--error')) {
                        wrapper.append('<div class="invalid-message">Please enter a valid phone number.</div>');
                    }
                    wrapper.addClass('ven-form-group--error');
                    countError = parseInt(countError) + 1;
                }
            }
        }
        else {
            if (!wrapper.hasClass('ven-form-group--error')) {
                wrapper.append('<div class="invalid-message">' + label + 'is required</div>');
            }
            wrapper.addClass('ven-form-group--error');
            countError = parseInt(countError) + 1;
            
        }

        return countError;
    }

    $(function () {
        
    });
})(jQuery);