(function ($) {
    function eventChangeQuanlity() {
        $(document).on('click', 'form.cart .ven-box-quantity .ven-box-quantity__minus', function () {
            var qtyBox = $(this).closest('.ven-box-quantity');

            var qty = qtyBox.find('input').val();

            qty = eval(qty) > 1 ? (eval(qty) - 1) : 1;

            qtyBox.find('input').val(qty);
        });

        $(document).on('click', 'form.cart .ven-box-quantity .ven-box-quantity__plus', function () {

            var qtyBox = $(this).closest('.ven-box-quantity');
            var qtyMax = qtyBox.find('input').attr('max');

            if(qtyMax == "") {
                qtyMax = 999;
            }

            var qty = qtyBox.find('input').val();
            qty = eval(qty) < 0 ? 1 : (eval(qty) < eval(qtyMax)  ? (eval(qty) + 1) : qty);
            
            qtyBox.find('input').val(qty);
        });
    }

   	function addToCart() {
        if ($('form.cart').length < 1) return;

        $('form.cart').on('submit', function (event) {
            event.preventDefault();
            var form = $(this);
            var data = form.serialize();

            $.ajax({
                //url: wp_vars.ajax_url,
                url:  wp_vars.home_url + '/?wc-ajax=add_to_cart',
                type: 'POST',
                //dataType: 'json',
                cache: false,
                data: data,
                beforeSend: function (xhr) {                    
                    form.find('button[type="submit"]').addClass('is-loading');
                }
            }).done(function (res) {
                form.find('button[type="submit"]').removeClass('is-loading');

                if(res.error && res.error == true) {
                    var message = 'This product cannot be added to cart';
                    $.ven_noti(message, 3000);
                } else {

                    $(document.body).trigger('wc_fragment_refresh');
                    form.find('.ven-btn-add-cart').addClass('is-active');

                    setTimeout(function(){ 
                        form.find('.ven-btn-add-cart').removeClass('is-active');
                    }, 2000);

                    var title = (typeof res.data.title != 'undefined') ? res.data.title : '';
                    if(title != '') {
                        $('#cart-notification-content .text-gradient').html(title);
                        showCartNotification();
                    } else {
                        var message = 'This product added to cart.';
                        $.ven_noti(message, 3000);
                    }
                }
                
                /*$(document.body).trigger('wc_fragment_refresh');

                var title = (typeof res.data.title != 'undefined') ? res.data.title : '';
                if(title != '') {
                	$('#cart-notification-content .text-gradient').html(title);
                	showCartNotification();
                } else {
                	var message = 'This product added to cart.';
                	$.ven_noti(message, 3000);
                }*/

            }).fail(function (res) {
            	var message = (typeof res.responseJSON.data.message != 'undefined') ? res.responseJSON.data.message : 'This product cannot be added to cart';
                var messageElement = $('<div class="message-element">' + message + '</div>');

                messageElement.find('a').remove();
                message = messageElement.text();

                form.find('button[type="submit"]').removeClass('is-loading');
             	$.ven_noti(message, 3000);
            });
        });
    }

    function buyNow() {
        if ($('form.cart').length < 1) return;

        $(document).on('click', 'form.cart .btn-buy-now', function(event) {

        	event.preventDefault();
        	var element = $(this);
            var form = element.closest('form.cart');
            var redirect = element.data('redirect');
            var productID = form.find('input[name="product_id"]').val()

            var data = {
            	action: 'ven_add_to_cart',
            	quantity: 1,
            	product_id: productID
            };

            $.ajax({
                //url: wp_vars.ajax_url,
                url:  wp_vars.home_url + '/?wc-ajax=add_to_cart',
                type: 'POST',
                //dataType: 'json',
                cache: false,
                data: data,
                beforeSend: function (xhr) {                    
                    element.addClass('is-loading');
                }
            }).done(function (res) {
                if (redirect != "") {
                	window.location.href = redirect;
                } else {
                	element.removeClass('is-loading');

                	var title = (typeof res.data.title != 'undefined') ? res.data.title : '';
	                if(title != '') {
	                	$('#cart-notification-content .text-gradient').html(title);
	                } else {
	                	var message = 'This product added to cart.';
	                	$.ven_noti(message, 3000);
	                }
                }

            }).fail(function (res) {
            	element.removeClass('is-loading');

                var message = (typeof res.responseJSON.data.message != 'undefined') ? res.responseJSON.data.message : 'This product cannot be added to cart';
                var messageElement = $('<div class="message-element">' + message + '</div>');

                messageElement.find('a').remove();
                message = messageElement.text();
                $.ven_noti(message, 3000);
            });
        });
    }

    $(function () {
        eventChangeQuanlity();
        addToCart();
        buyNow();
    });
})(jQuery);