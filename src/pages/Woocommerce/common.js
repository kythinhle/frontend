function initWooSelect2() {
    $('.ven-form-group select').select2({
        width: "100%"
    });
}

(function ($) {
    $(function () {
        initWooSelect2();
    });
})(jQuery);