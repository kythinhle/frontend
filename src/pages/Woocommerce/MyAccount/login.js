(function ($) {
    var isLogin = false;
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) { return; }
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    /* Init Login Facebook */
    window.fbAsyncInit = function () {
        FB.init({
            appId: '3349125235150681',
            cookie: true,  // Enable cookies to allow the server to access the session.
            xfbml: true,  // Parse social plugins on this webpage.
            version: 'v5.0' // Use this Graph API version for this call.
        });
    };

    $(function () {

        $('.ec-btn-facebook').on('click', function () {
            if (isLogin) { return; }
            isLogin = true;
            FB.login(function (response) {
                if (response.status === 'connected') {
                    facebook_login_connected_callback();
                }
            }, { scope: 'public_profile,email' });
        })

        /* Call Ajax Register Login By Facebook */
        function facebook_login_connected_callback() {
            FB.api('/me', { fields: 'id,name,first_name,last_name,picture,verified,email' }, function (response) {

                var provide_id = '';
                var first_name = '';
                var last_name = '';
                var name = '';
                var email = '';
                var picture = '';

                if (typeof response.id === "undefined") {
                    console.log("Can not get provide id ");
                    isLogin = false;
                    return false;
                } else {
                    provide_id = response.id;
                }

                if (typeof response.first_name !== "undefined") {
                    first_name = response.first_name;
                }

                if (typeof response.last_name !== "undefined") {
                    last_name = response.last_name;
                }

                if (typeof response.name !== "undefined") {
                    name = response.name;
                }

                if (typeof response.email === "undefined") {
                    console.log("Can not get provide id ");
                    isLogin = false;
                    return false;
                } else {
                    email = response.email;
                }

                if (typeof response.picture.data.url !== "undefined") {
                    picture = response.picture.data.url;
                }

                jQuery.ajax({
                    url: wp_vars['rest_url'] + 'api/v1/auth/register',
                    type: 'POST',
                    cache: false,
                    data: {
                        "type": "facebook",
                        "id": provide_id,
                        "first_name": first_name,
                        "last_name": last_name,
                        "display_name": name,
                        "email": email,
                        "picture": picture
                    },
                }).done(function (response) {
                    location.reload();
                }).fail(function (res) {
                    isLogin = false;

                    var message = (typeof res.responseJSON.message != 'undefined') ? res.responseJSON.message : '';

                    if (message !== "") {
                        $.ven_noti(message, 3000);
                    }

                });

            });
        }

        $('#form-login').on('submit', function (event) {
            event.preventDefault();
            var form = $(this);
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var message = '';

            var email = form.find('input[name="email"]');
            var password = form.find('input[name="password"]');

            form.find('.ven-form-group--error').removeClass('ven-form-group--error');
            form.find('.invalid-message').remove();

            if (email.val() == "") {
                message = 'Email address is required.';
                email.closest(".ven-form-group").addClass('ven-form-group--error');
                email.closest(".ven-form-group").append('<div class="invalid-message">' + message + '</div>');
                return false;
            } else if (!regex.test(email.val())) {
                message = 'Please enter a valid email address.';
                email.closest(".ven-form-group").addClass('ven-form-group--error');
                email.closest(".ven-form-group").append('<div class="invalid-message">' + message + '</div>');
                return false;
            }

            if (password.val() == "") {
                message = 'Password is required.';
                password.closest(".ven-form-group").addClass('ven-form-group--error');
                password.closest(".ven-form-group").append('<div class="invalid-message">' + message + '</div>');
                return false;
            }

            var data = form.serialize();

            $.ajax({
                url: wp_vars['rest_url'] + 'api/v1/auth/login',
                type: 'POST',
                dataType: 'json',
                cache: false,
                data: data,
                beforeSend: function (xhr) {
                    form.find('.is-loading').removeClass('is-loading');
                    form.find('button').addClass('is-loading');
                }
            }).done(function (response) {
                var formdata = {};
                form.serializeArray().map(function (x) {
                    formdata[x.name] = x.value;
                });

                form.find('.is-loading').removeClass('is-loading');

                var url_referer = typeof formdata.url_referer !== 'undefined' && formdata.url_referer != '' ? formdata.url_referer : '';
                if (url_referer != "") {
                    window.location.href = url_referer;
                } else {
                    location.reload();
                }

            }).fail(function (res) {
                form.find('.is-loading').removeClass('is-loading');
                var message = (typeof res.responseJSON.message != 'undefined') ? res.responseJSON.message : '';

                if (message !== "") {
                    $.ven_noti(message, 3000);
                }

            });;
        });
    });


})(jQuery);