(function ($) {
	function signUpWidthEmail() {
		if ($('#form-register').length < 1) return;

		$('#form-register').on('submit', function (event) {
			event.preventDefault();
			var form = $(this);
			var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			var message = '';

			var email = form.find('input[name="email"]');
			var password = form.find('input[name="password"]');
			var confPassword = form.find('input[name="conf-password"]');

			form.find('.ven-form-group--error').removeClass('ven-form-group--error');
			form.find('.invalid-message').remove();

			if (email.val() == "") {
				message = 'Email address is required.';
				email.closest(".ven-form-group").addClass('ven-form-group--error');
				email.closest(".ven-form-group").append('<div class="invalid-message">' + message + '</div>');
				return false;
			} else if (!regex.test(email.val())) {
				message = 'Please enter a valid email address.';
				email.closest(".ven-form-group").addClass('ven-form-group--error');
				email.closest(".ven-form-group").append('<div class="invalid-message">' + message + '</div>');
				return false;
			}

			if (password.val() == "") {
				message = 'Password is required.';
				password.closest(".ven-form-group").addClass('ven-form-group--error');
				password.closest(".ven-form-group").append('<div class="invalid-message">' + message + '</div>');
				return false;
			}

			if (confPassword.val() == "") {
				message = 'Confirm password is required.';
				confPassword.closest(".ven-form-group").addClass('ven-form-group--error');
				confPassword.closest(".ven-form-group").append('<div class="invalid-message">' + message + '</div>');
				return false;
			} else if (confPassword.val() != password.val()) {
				message = 'Password and confirm password does not match';
				confPassword.closest(".ven-form-group").addClass('ven-form-group--error');
				confPassword.closest(".ven-form-group").append('<div class="invalid-message">' + message + '</div>');
				return false;
			}

			var data = form.serialize();

			$.ajax({
				url: wp_vars['rest_url'] + 'api/v1/auth/register',
				type: 'POST',
				dataType: 'json',
				cache: false,
				data: data,
				beforeSend: function (xhr) {
					form.find('.is-loading').removeClass('is-loading');
					form.find('button').addClass('is-loading');
				}
			}).done(function (response) {

				var formdata = {};
				form.serializeArray().map(function (x) {
					formdata[x.name] = x.value;
				});

				var url_referer = typeof formdata.url_referer !== 'undefined' && formdata.url_referer != '' ? formdata.url_referer : '';

				form.find('.is-loading').removeClass('is-loading');

				if (url_referer != "") {
					window.location.href = url_referer;
				} else {
					location.reload();
				}
			}).fail(function (res) {
				form.find('.is-loading').removeClass('is-loading');
				var message = (typeof res.responseJSON.message != 'undefined') ? res.responseJSON.message : '';

				if (message !== "") {
					$.ven_noti(message, 3000);
				}

			});;
		});
	}

	$(function () {
		signUpWidthEmail();
	});
})(jQuery);