(function ($) {
    function eventChangeQuanlity() {
        $(document).on('click', 'body.woocommerce-cart .ven-box-quantity .ven-box-quantity__minus', function () {
            var qtyBox = $(this).closest('.ven-box-quantity');

            var qty = qtyBox.find('input').val();

            qty = eval(qty) > 1 ? (eval(qty) - 1) : 1;

            qtyBox.find('input').val(qty);

            triggerUpdateCart()
        });

        $(document).on('click', 'body.woocommerce-cart .ven-box-quantity .ven-box-quantity__plus', function () {

            var qtyBox = $(this).closest('.ven-box-quantity');
            var qtyMax = qtyBox.find('input').attr('max');

            if (qtyMax == "") {
                qtyMax = 999;
            }

            var qty = qtyBox.find('input').val();
            qty = eval(qty) < 0 ? 1 : (eval(qty) < eval(qtyMax) ? (eval(qty) + 1) : qty);

            qtyBox.find('input').val(qty);

            triggerUpdateCart();
        });

        $(document).on('change', 'body.woocommerce-cart .ven-box-quantity input', function () {
            if ($(this).val() < 1) {
                $(this).val(1);
            }

            triggerUpdateCart();
        });
    }

    function triggerUpdateCart() {
        $("#update_cart").removeAttr('disabled');
        $("#update_cart").trigger('click');
    }

    $(function () {
        eventChangeQuanlity();
    });
})(jQuery);