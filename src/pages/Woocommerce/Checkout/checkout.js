(function ($) {
    var buttonOrder = $('#checkout-order-trigger'),
        checkoutContent = $('.ven-checkout__content'),
        countError = 0;

    function initTermsBoxScrollbar() {
        var elBox = $('.ven-checkout-terms-box');
        if (elBox.length) {
            elBox.addClass('scrollbar-macosx');
            elBox.scrollbar();
        }
    }

    function handleCheckoutSteps() {
        $(document).on('click', '.checkout-next-step-trigger', function() {
            var currentStep = $(this).closest('.ven-checkout-steps__item'),
                blockForm = $('.ven-checkout-steps__form', currentStep),
                blockData = $('.ven-checkout-steps__data', currentStep);

            if(currentStep.data('type') == 'shipping') {
                countError = 0;

                if ($('.ven-checkout-steps__item[data-type="shipping"] .validate-required').length > 0) {
                    $('.ven-checkout-steps__item[data-type="shipping"] .validate-required').each(function () {
                       var thisInput = $(this);
                        countError = $.validateInput(thisInput, countError);

                    });
                }

                if (countError > 0) {
                    $(".show-shipping-normal").trigger("click");
                    return;
                }

                var shippingFirstName = $("#shipping_first_name").val(),
                    shippingLastName = $("#shipping_last_name").val(),
                    shippingCompany = $("#shipping_company").val(),
                    shippingPhone = $("#shipping_phone").val(),
                    shippingAddress = $("#shipping_address_1").val(),
                    //shippingUnit = $("#shipping_address_2").val(),
                    shippingCity = $("#shipping_city").val(),
                    shippingPostcode = $("#shipping_postcode").val(),
                    shippingState = $("#shipping_state :selected").text();

                if($("#shipping_country").is('input')) {
                    var shippingCountry = $("#shipping_country").closest('.woocommerce-input-wrapper').find('strong').text();
                } else {
                    var shippingCountry = $("#shipping_country :selected").text();
                }

                var addressShipto = shippingAddress + (shippingAddress.length > 0 ? ", " : "") + shippingCity + (shippingCity.length > 0 ? ", " : "") + shippingState + (shippingState.length > 0 && shippingPostcode.length < 1 ? ", " : " ") + shippingPostcode + (shippingPostcode.length > 0 ? ", " : "") + shippingCountry;

                var fullName = shippingFirstName + ' ' + shippingLastName;

                $('#shipping-info').html(fullName + (shippingCompany.length > 0 ? '<br>' + shippingCompany : shippingCompany) + '<br>' + addressShipto + '<br>' + shippingPhone);

                $(document.body).trigger('update_checkout');
            } else if( currentStep.data('type') == 'shipping-method') {
                if ($('.shipping_method:checked').length > 0) {
                    var shipping_label = $('.shipping_method:checked').closest('.ven-checkout-delivery__item').find('label .ven-checkout-delivery__title').length > 0 ? $('.shipping_method:checked').closest('.ven-checkout-delivery__item').find('label .ven-checkout-delivery__title').html() : '';

                    var shipping_price = $('.shipping_method:checked').closest('.ven-checkout-delivery__item').find('label .ven-checkout-delivery__price').length > 0 ? $('.shipping_method:checked').closest('.ven-checkout-delivery__item').find('label .ven-checkout-delivery__price').html() : '';

                    var shipping_description = $('.shipping_method:checked').closest('.ven-checkout-delivery__item').find('label .ven-checkout-delivery__description').length > 0 ? $('.shipping_method:checked').closest('.ven-checkout-delivery__item').find('label .ven-checkout-delivery__description').html() : '';


                    $('#shipping-method-review').html(shipping_label + ' ' + (shipping_description.length > 0 ? '('+ shipping_description +') ' : '') + shipping_price);
                }

            }
            
            handleCheckoutNextStep(currentStep, blockForm, blockData);
        });

        $(document).on('click', '.checkout-edit-step-trigger', function() {
            var currentStep = $(this).closest('.ven-checkout-steps__item'),
                blockForm = $('.ven-checkout-steps__form', currentStep),
                blockData = $('.ven-checkout-steps__data', currentStep);

                handleCheckoutEditStep(currentStep, blockForm, blockData);
        });

        /*$('.ven-checkout-steps__item').each(function () {
            var currentStep = $(this),
                buttonNext = $('.checkout-next-step-trigger', currentStep),
                buttonEdit = $('.checkout-edit-step-trigger', currentStep),
                blockForm = $('.ven-checkout-steps__form', currentStep),
                blockData = $('.ven-checkout-steps__data', currentStep);

            buttonEdit.on('click', function () {
                handleCheckoutEditStep(currentStep, blockForm, blockData);
            });

            buttonOrder.on('click', function () {
                handleCheckoutValidator();
            });
        });*/
    }

    function handleCheckoutNextStep(currentStep, currentForm, currentData) {
        currentStep.addClass('is-checked');
        $('.ven-checkout-steps__item.is-checked').last().next().find('.ven-checkout-steps__body').slideDown(300);
        currentForm.slideUp(300);
        currentData.slideDown(300);
        $('.ven-checkout-steps__item').removeClass('is-current');
        $('.ven-checkout-steps__item.is-checked').last().next().addClass('is-current');
    }

    function handleCheckoutEditStep(currentStep, currentForm, currentData) {
        $('.ven-checkout-steps__item').removeClass('is-current');
        $('.ven-checkout-steps__item:not(".is-checked")').find('.ven-checkout-steps__body:visible').slideUp(300);
        $('.ven-checkout-steps__item.is-checked .ven-checkout-steps__data').show();
        $('.ven-checkout-steps__item.is-checked .ven-checkout-steps__form').hide();
        currentStep.addClass('is-current');
        currentForm.slideDown(300);
        currentData.slideUp(300);
    }

    function handleCheckoutValidator() {
        $('body').addClass('is-loading-overlay is-lock');
    }

    function toggleBillingAddress() {
        var billingAddressCheckBox = $('#checkout-billing-address'),
            billingAddressContent = $('.ven-checkout-billing__body');

        if (billingAddressCheckBox.prop("checked")) {
            setBillingCountry();
        }

        billingAddressCheckBox.prop('checked') ? billingAddressContent.hide() : billingAddressContent.show();
        billingAddressCheckBox.on('change', function () {
            billingAddressCheckBox.prop('checked') ? billingAddressContent.slideUp(300) : billingAddressContent.slideDown(300);
        });
    }

    function toggleCheckoutDetail() {
        var elToggleTrigger = $('.ven-checkout__summary'),
            elToggleContent = $('.ven-checkout__sidebar');
        elToggleTrigger.on('click', function () {
            $(this).toggleClass('is-active');

            if ($(this).hasClass('is-active')) {
                elToggleContent.stop().slideDown(300);
            }
            else {
                elToggleContent.stop().slideUp(300);
            }
        });
    }

    /**
     * Apply coupon
     */
    function applyCoupon() {
        $(document).on('click', '#btn_coupon_code_edit', function (e) {
            e.preventDefault();

            var coupon = $("#coupon_code_edit").val(),
                url = $("form.woocommerce-form-coupon-edit").data("url") + "/?wc-ajax=apply_coupon",
                security_code = $("form.woocommerce-form-coupon-edit").data("nonce");
            if (coupon.length < 1) { return; }
            $.post(url, { coupon_code: coupon, security: security_code }, function (res) {
                $(document.body).trigger('update_checkout');
                $.ven_noti(res, 3000);
            });
        });
    }

    function showShippingManual() {
        $(".show-shipping-normal").on('click', function (e) {
            e.preventDefault();
            initSelect2('#shipping_state');

            $(".ven-form-manual").removeClass("d-none");
            $("#shipping_address_google").closest(".form-row").addClass("d-none");

            $(".ven-checkout-shipping-information").remove(); 

            var formFields = $('.gfield, .ven-form-group, .woocommerce-form-row');
            formFields.each(function () {
                var field = $(this);
                var input = field.find('input, textarea');
                var label = field.find('label');

                input.focus(function () {
                    label.addClass('freeze');
                });

                input.focusout(function () {
                    checkInput();
                });

                if (input.val() && input.val().length) {
                    label.addClass('freeze');
                }

                function checkInput() {
                    var valueLength = input.val().length;

                    if (valueLength > 0) {
                        label.addClass('freeze');
                    } else {
                        label.removeClass('freeze');
                    }
                }

                input.change(function () {
                    checkInput();
                });
            });
        })
    }

    function initSelect2(element) {
        if ($(element).is('select')) {
            $(element).select2({
               width: "100%",
                minimumResultsForSearch: -1,
            });
        }
    }

    /**
     * Initialize google map
     */
    function initGoogleMap() {
        if($("#google-map").length > 0) {
            $("#google-map").attr("src", $("#google-map").attr("data-src"));   
        }             
    }

     /**
     * Autocomplate address
     */
    function initAutocomplateAddress (element) {
        if (element.length < 1) return;

        //Location Lookup
        var input = element[0],
            autocomplete;
        (function pacSelectFirst(input) {
            // store the original event binding function
            var _addEventListener = (input.addEventListener) ? input.addEventListener : input.attachEvent;

            function addEventListenerWrapper(type, listener) {
                if (type == "keydown") {
                    var orig_listener = listener;
                    listener = function (event) {
                        var suggestion_selected = $(".pac-item-selected").length > 0;
                        if (event.which == 13 && !suggestion_selected) {
                            var simulated_downarrow = $.Event("keydown", {
                                keyCode: 40,
                                which: 40
                            });
                            orig_listener.apply(input, [simulated_downarrow]);
                        }
                        orig_listener.apply(input, [event]);
                    };
                }
                _addEventListener.apply(input, [type, listener]);
            }
            input.addEventListener = addEventListenerWrapper;
            input.attachEvent = addEventListenerWrapper;
            autocomplete = new google.maps.places.Autocomplete(input, {
                componentRestrictions: {
                    country: $('#shipping_country').length > 0 ? $('#shipping_country').val() : 'au',
                }
            });
        })(input);

        autocomplete.addListener('place_changed', function () {
            var place = autocomplete.getPlace();
            var adr_address = place.adr_address;
            var formatted_address = place.formatted_address;
            var name = place.name;

            if (!place.geometry) {
                return;
            }

            var premise, 
                subpremise, 
                street_number, 
                route, 
                locality, 
                administrative_area_level_1, 
                administrative_area_level_1_short,
                administrative_area_level_2, 
                administrative_area_level_2_short, 
                postal_code, 
                country, 
                country_short, 
                address = '', 
                address_2 = '';

            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];

                if (addressType == "premise") {
                    premise = place.address_components[i].long_name;
                } else if (addressType == "subpremise") {
                    subpremise = place.address_components[i].long_name;
                } else if (addressType == "street_number") {
                    street_number = place.address_components[i].long_name;
                } else if (addressType == "route") {
                    route = place.address_components[i].long_name;
                } else if (addressType == "locality") {
                    locality = place.address_components[i].long_name;
                } else if (addressType == "administrative_area_level_1") {
                    administrative_area_level_1 = place.address_components[i].long_name;
                    administrative_area_level_1_short = place.address_components[i].short_name;
                } else if (addressType == "administrative_area_level_2") {
                    administrative_area_level_2 = place.address_components[i].long_name;
                    administrative_area_level_2_short = place.address_components[i].short_name;
                } else if (addressType == "postal_code") {
                    postal_code = place.address_components[i].long_name;
                } else if (addressType == "country") {
                    country = place.address_components[i].long_name;
                    country_short = place.address_components[i].short_name;
                }
            }

            if (typeof subpremise != 'undefined') {
                address += subpremise + '/';
            }

            if (typeof premise != 'undefined') {
                address += premise + ' ';
            }

            if (typeof street_number != 'undefined') {
                address += street_number + ' ';
            }

            if (typeof route != 'undefined') {
                address += route;
            }

            $("#shipping_address_1").val(address);

            if($("#shipping_address_2").length > 0) {
                $("#shipping_address_2").val(address_2)
            }

            //element.val(formatted_address);
            if ($("#shipping_state_field").attr('style') == 'display: block;') {
                if (typeof administrative_area_level_1 != 'undefined') {
                    if ($("#shipping_state").is('input')) {
                        $("#shipping_state").val(administrative_area_level_1);
                    } else {
                        $("#shipping_state").val(administrative_area_level_1_short).trigger('change');
                    }
                }
            } else {
                if (typeof administrative_area_level_1 != 'undefined') {

                    if (typeof administrative_area_level_2 != 'undefined') {
                        administrative_area_level_1 = administrative_area_level_1.length > 0 ? (administrative_area_level_2 + ', ' + administrative_area_level_1) : administrative_area_level_2;
                    }

                    $("#shipping_city").val(administrative_area_level_1);
                } else {
                    $("#shipping_city").val("");
                }
            }

            if (typeof locality != 'undefined') {
                $("#shipping_city").val(locality);
            }

            if (typeof postal_code != 'undefined') {
                $("#shipping_postcode").val(postal_code);
            } else {
                $("#shipping_postcode").val("");
            }

            //Open manual input after selected address
            //$(".show-shipping-normal").trigger("click");
        });

        $(document).on('change', '#shipping_country', function () {

            if ($(this).val() != "") {
                setBillingCountry();
                
                autocomplete.setComponentRestrictions({
                    'country': $(this).val()
                });
            }
        });

        $(document).on('change', '#shipping_state', function () {
            if ($(this).val() != "") {
                setBillingCountry();                
            }
        });
    }

    function setBillingCountry() {
        $('.ven-form-group select').closest('.ven-form-group').addClass('has-select');
        
        var shipping = $('#shipping_country').val();
        var shipping_state = $('#shipping_state').val();

        if($("#checkout-billing-address:checked").length > 0) {
            $('#billing_country').val(shipping).trigger('change');
            $('#billing_state').val(shipping_state).trigger('change');
        }
    }

    function guestCheckout() {
        $('.continue-as-guest').click(function() {
            $('.ven-user-form').addClass('d-none');
            $('.checkout.woocommerce-checkout').removeClass('d-none');
        });
    }

    function switchCheckoutLogin() {
        $('.checkout-login-target').on('click', function(e) {
            e.preventDefault();
            var $target = $(this).attr('href');
            $('.ven-checkout-login.is-active').removeClass('is-active');
            $($target).addClass('is-active');
        });
    }

    $(function () {
        initGoogleMap();
        $("#google-map").on("load", function() {
            initAutocomplateAddress($("#shipping_address_google"));
        });
        initTermsBoxScrollbar();
        handleCheckoutSteps();
        toggleBillingAddress();
        toggleCheckoutDetail();
        setBillingCountry();
        applyCoupon();
        showShippingManual();
        guestCheckout();
        switchCheckoutLogin();
    })
})(jQuery);