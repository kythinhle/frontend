(function ($) {
    function initCartPopupScrollbar() {
        $('.ven-cart-popup__inner').addClass('scrollbar-macosx').scrollbar();
    }

    $(function () {
        initCartPopupScrollbar();
    });
})(jQuery);