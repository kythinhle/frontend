(function ($) {
    function handleNavCollapse() {
        $('.navbar-toggler').on('click', function () {
            $(this).toggleClass('show');
            $('.navbar').toggleClass('show');
            $('body').toggleClass('is-lock');
        });
    }

    function handleNavbarFixed() {
        var navbar = $('.navbar');
        $(window).on('scroll', function () {
            if ($(window).scrollTop() > navbar.outerHeight()) {
                navbar.addClass('is-active');
            }
            else {
                navbar.removeClass('is-active');
            }
        });
    }

    $(function () {
        handleNavCollapse();
        handleNavbarFixed();
    });
})(jQuery);